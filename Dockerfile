FROM kalilinux/kali-rolling

ENV DEBIAN_FRONTEND noninteractive

RUN apt update && \
    apt dist-upgrade -yqq && \
    apt install -yqq wget kali-linux-headless kali-desktop-xfce xorg xrdp
